```jsx

<Input placeholder="Basic usage" />
```

This is simple input component that uses the same [input](https://ant.design/components/input) from ant-design with little changes to
password input.




## props:

| Prop |Information|Type |Default |
| -----|-----------|-----|--------|
| type | The type of html input | string | text |
| disabled | Whether the input is disabled. | boolean | false |
| id | 	The ID for input | string | N/A |
| value | The input content value | string | N/A |
| onChange | callback when user input | function(e) | N/A |


>**Note** : For more information about the complete list of props you can go [here](https://ant.design/components/input/#API).



