"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactSlick = _interopRequireDefault(require("react-slick"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _tag = _interopRequireDefault(require("antd/lib/tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var TagList = function TagList(_ref) {
  var slidesToScroll = _ref.slidesToScroll,
      slidesToShow = _ref.slidesToShow,
      infinite = _ref.infinite,
      autoPlay = _ref.autoPlay,
      autoPlaySpeed = _ref.autoPlaySpeed,
      listOfTags = _ref.listOfTags,
      props = _objectWithoutProperties(_ref, ["slidesToScroll", "slidesToShow", "infinite", "autoPlay", "autoPlaySpeed", "listOfTags"]);

  var settings = {
    infinite: infinite,
    slidesToShow: slidesToShow,
    slidesToScroll: slidesToScroll,
    autoPlay: autoPlay,
    autoPlaySpeed: autoPlaySpeed,
    responsive: [{
      breakpoint: 968,
      settings: {
        slidesToShow: slidesToShow - 1,
        slidesToScroll: slidesToScroll - 1
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: slidesToShow - 2,
        slidesToScroll: slidesToScroll - 2,
        initialSlide: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: slidesToShow - 3,
        slidesToScroll: slidesToScroll - 3
      }
    }]
  };
  return _react["default"].createElement("div", null, _react["default"].createElement(_reactSlick["default"], _extends({
    className: "slider-container"
  }, settings), listOfTags.map(function (itemList) {
    return _react["default"].createElement(_tag["default"], _extends({}, props, {
      key: itemList,
      className: "tags-container"
    }), itemList);
  })));
};

TagList.propTypes = {
  slidesToScroll: _propTypes["default"].number,
  slidesToShow: _propTypes["default"].number,
  infinite: _propTypes["default"].bool,
  autoPlay: _propTypes["default"].bool,
  autoPlaySpeed: _propTypes["default"].number,
  listOfTags: _propTypes["default"].array.isRequired
};
TagList.defaultProps = {
  infinite: false,
  slidesToShow: 6,
  slidesToScroll: 4,
  autoPlay: false,
  autoPlaySpeed: 0
};
var _default = TagList;
exports["default"] = _default;