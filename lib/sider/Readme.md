```js
<Layout>
    <Sider
      collapsible
      theme="primary"
      style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }}
    >
      <div className="logo" />
      <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline">
        <Menu.Item key="1" href="#components-anchor-demo-basic">
          <Icon type="pie-chart" className="menu-icon" />
          <span>Option 1</span>
        </Menu.Item>
        <Menu.Item key="2">
          <Icon type="desktop" className="menu-icon" />
          <span>Option 2</span>
        </Menu.Item>
        <Menu.Item key="3">
          <Icon type="user" className="menu-icon" />
          <span>User</span>
        </Menu.Item>
        <Menu.Item key="4">
          <Icon type="team" className="menu-icon" />
          <span>Team</span>
        </Menu.Item>
        <Menu.Item key="5">
          <Icon type="file" className="menu-icon" />
          <span>File</span>
        </Menu.Item>
      </Menu>
    </Sider>
    <Layout style={{ marginLeft: 200 }}>
      <Header style={{ background: '#fff', padding: 0 }} />
      <Content style={{ margin: '0 16px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>User</Breadcrumb.Item>
          <Breadcrumb.Item>Bill</Breadcrumb.Item>
        </Breadcrumb>
        <div className="bg-lighter  container">
          content
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>FluAnt Design ©2018</Footer>
    </Layout>
  </Layout>
```