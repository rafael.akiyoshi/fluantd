"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _sider["default"];
  }
});

var _sider = _interopRequireDefault(require("./sider"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }