"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _card = _interopRequireDefault(require("antd/lib/card"));

require("antd/lib/card/style");

var _classnames = _interopRequireDefault(require("classnames"));

var _header = _interopRequireDefault(require("./header"));

var _body = _interopRequireDefault(require("./body"));

var _footer = _interopRequireDefault(require("./footer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var classPrefix = 'fluantd-card';

var Card = function Card(props) {
  var background = props.background,
      className = props.className,
      restProps = _objectWithoutProperties(props, ["background", "className"]);

  var classnames = (0, _classnames["default"])(classPrefix, className);
  return _react["default"].createElement(_card["default"], _extends({}, restProps, {
    className: classnames
  }));
};

var Meta = Card.Meta,
    Grid = Card.Grid;
Card.Meta = Meta;
Card.Grid = Grid;
Card.Header = _header["default"];
Card.Body = _body["default"];
Card.Footer = _footer["default"];
var _default = Card;
exports["default"] = _default;