"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _button = _interopRequireDefault(require("../button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var RecordButton = function RecordButton(_ref) {
  var buttonSize = _ref.buttonSize,
      iconSize = _ref.iconSize,
      disabled = _ref.disabled,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, ["buttonSize", "iconSize", "disabled", "className"]);

  var buttonStyle = {
    height: "".concat(buttonSize, "px"),
    width: "".concat(buttonSize, "px")
  };
  var iconStyle = {
    height: "".concat(iconSize, "px"),
    width: "".concat(iconSize, "px")
  };
  var classes = (0, _classnames["default"])('record', className);
  return _react["default"].createElement(_button["default"], _extends({
    style: buttonStyle,
    shape: "circle",
    disabled: disabled,
    className: classes
  }, props), _react["default"].createElement("img", {
    style: iconStyle,
    src: "../../theme/icons/recordings.svg",
    alt: ""
  }));
};

RecordButton.propTypes = {
  buttonSize: _propTypes["default"].number,
  iconSize: _propTypes["default"].number,
  disabled: _propTypes["default"].bool
};
RecordButton.defaultProps = {
  buttonSize: 63,
  iconSize: 43,
  disabled: false
};
var _default = RecordButton;
exports["default"] = _default;