"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _col["default"];
  }
});

require("antd/lib/col/style");

var _col = _interopRequireDefault(require("antd/lib/col"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }