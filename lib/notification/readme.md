``` jsx
    const message = 'Notification Title';
    const description = 'This is the content of the notification. This is the content of the notification. This is the content of the notification.';
    notifSuccess = () => {
        const options = {
          type: 'success',
          message,
          description
        };
        Notification(options);
      };
    
      notifWarning = () => {
        const options = {
          type: 'warning',
          message,
          description
        };
        Notification(options);
      };
    
      notifInfo = () => {
        const options = {
          type: 'info',
          message,
          description
        };
        Notification(options);
      };
    
      notifError = () => {
        const options = {
          type: 'error',
          message,
          description,
          duration: 0
        };
        Notification(options);
      };
      <div>
        <Button type="alert" onClick={this.notifWarning}>Alert</Button>
        <Button type="primary" onClick={this.notifInfo}>Info</Button>
        <Button type="success" onClick={this.notifSuccess}>Success</Button>
        <Button type="danger" onClick={this.notifError}>Error</Button>
      </div>
```
