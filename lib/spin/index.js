"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _spin["default"];
  }
});

require("antd/lib/spin/style");

var _spin = _interopRequireDefault(require("antd/lib/spin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }