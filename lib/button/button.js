"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _button = _interopRequireDefault(require("antd/lib/button"));

var _classnames = _interopRequireDefault(require("classnames"));

var _propTypes = _interopRequireDefault(require("prop-types"));

require("antd/lib/button/style");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Button = function Button(_ref) {
  var _classNames;

  var type = _ref.type,
      shape = _ref.shape,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, ["type", "shape", "className"]);

  var classes = (0, _classnames["default"])(className, (_classNames = {}, _defineProperty(_classNames, "ant-btn-".concat(type), type), _defineProperty(_classNames, "ant-btn-".concat(shape), shape), _classNames));
  return _react["default"].createElement(_button["default"], _extends({}, props, {
    className: classes
  }));
};

Button.propTypes = {
  /** the type could be one of  : primary , danger , alert,  success */
  type: _propTypes["default"].string,

  /** the shape could be one of  : rounded , rounded-outline */
  shape: _propTypes["default"].string,
  ghost: _propTypes["default"].bool,
  disabled: _propTypes["default"].bool
};
Button.Group = _button["default"].Group;
var _default = Button;
exports["default"] = _default;