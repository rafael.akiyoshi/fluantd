"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _tooltip = _interopRequireDefault(require("antd/lib/tooltip"));

var _classnames = _interopRequireDefault(require("classnames"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/**
 * StatusBar Component is used for explicit inform the
 * user about the situation of something, using colors.
 */
var StatusBar = function StatusBar(_ref) {
  var info = _ref.info,
      placement = _ref.placement,
      hoverable = _ref.hoverable,
      color = _ref.color,
      width = _ref.width,
      height = _ref.height,
      status = _ref.status,
      borderRadius = _ref.borderRadius,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, ["info", "placement", "hoverable", "color", "width", "height", "status", "borderRadius", "className"]);

  if (hoverable) {
    hoverable = 'hoverable-';
  } else {
    hoverable = '';
  }

  var classes = (0, _classnames["default"])(className, _defineProperty({}, "fluantd-".concat(hoverable).concat(status), status));
  var StatusBarStyle = {
    width: width,
    background: color,
    height: height,
    borderRadius: borderRadius
  };
  return _react["default"].createElement(_tooltip["default"], {
    placement: placement,
    title: info,
    arrowPointAtCenter: true
  }, _react["default"].createElement("div", _extends({
    className: classes,
    style: StatusBarStyle
  }, props)));
};

StatusBar.propTypes = {
  info: _propTypes["default"].string,
  placement: _propTypes["default"].string,
  hoverable: _propTypes["default"].bool,
  color: _propTypes["default"].string,
  width: _propTypes["default"].string,
  height: _propTypes["default"].number,
  status: _propTypes["default"].string,
  borderRadius: _propTypes["default"].string,
  className: _propTypes["default"].string
};
StatusBar.defaultProps = {
  info: '',
  placement: 'top',
  hoverable: false,
  width: '100%',
  height: 8,
  status: 'primary',
  borderRadius: '100px'
};
var _default = StatusBar;
exports["default"] = _default;