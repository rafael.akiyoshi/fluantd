```js
<div>
  <StatusBar />
</div>
<br />
<div>
  <StatusBar hoverable status="error"/>
</div>
<br />
<StatusBar
  height={20}
  width="50%"
  info="some tooltip"
  placement="topLeft"
  borderRadius="20px"
/>
```