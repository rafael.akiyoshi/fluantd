"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _icon = _interopRequireDefault(require("antd/lib/icon"));

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Message =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Message, _React$PureComponent);

  function Message() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Message);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Message)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      visible: false,
      wasClosed: false
    });

    _defineProperty(_assertThisInitialized(_this), "openMessage", function () {
      _this.setState({
        visible: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closeMessage", function () {
      _this.setState({
        visible: false
      });

      _this.setState({
        wasClosed: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closeButton", function () {
      var _this$props = _this.props,
          closeType = _this$props.closeType,
          closePosition = _this$props.closePosition,
          active = _this$props.active;

      if (!active) {
        return _react["default"].createElement("span", {
          className: "close-".concat(closePosition),
          onClick: _this.closeMessage,
          role: "presentation",
          onKeyDown: _this.closeMessage
        }, closeType);
      }

      return null;
    });

    _defineProperty(_assertThisInitialized(_this), "openButton", function () {
      var _this$props2 = _this.props,
          openType = _this$props2.openType,
          className = _this$props2.className,
          active = _this$props2.active,
          color = _this$props2.color;
      var visible = _this.state.visible;
      var openTypeClass = openType ? 'byProps' : 'default';
      var visibleClass = visible ? 'visible' : 'invisible';
      var classes = (0, _classnames["default"])(className, _defineProperty({}, "open-".concat(openTypeClass, "-").concat(visibleClass), openTypeClass));

      if (!active) {
        if (openType) {
          return _react["default"].createElement("div", {
            className: classes
          }, _react["default"].createElement("span", {
            onClick: _this.openMessage,
            role: "presentation",
            onKeyDown: _this.openMessage
          }, openType));
        }

        return _react["default"].createElement("div", {
          className: className
        }, _react["default"].createElement("span", {
          role: "presentation",
          onClick: _this.openMessage,
          onKeyDown: _this.openMessage
        }, _react["default"].createElement(_icon["default"], {
          type: "question-circle",
          style: {
            background: color
          },
          className: classes
        })));
      }

      return null;
    });

    return _this;
  }

  _createClass(Message, [{
    key: "render",
    value: function render() {
      var _classNames2;

      var _this$props3 = this.props,
          children = _this$props3.children,
          active = _this$props3.active,
          fullScreen = _this$props3.fullScreen,
          color = _this$props3.color;
      var _this$state = this.state,
          visible = _this$state.visible,
          wasClosed = _this$state.wasClosed;
      var fullScreenClass = fullScreen ? 'fullScreen' : 'default';
      var activeClass = active ? 'active' : 'notActive';
      var wasClosedClass = wasClosed ? "wasClosed-".concat(fullScreenClass) : "wasNotClosed-".concat(fullScreenClass);
      var classes = (0, _classnames["default"])((_classNames2 = {}, _defineProperty(_classNames2, "fluant-message-".concat(fullScreenClass), fullScreenClass), _defineProperty(_classNames2, "fluant-message-".concat(activeClass), activeClass), _classNames2));

      if (visible) {
        return _react["default"].createElement("div", null, this.openButton(), _react["default"].createElement("div", {
          className: classes,
          style: {
            background: color
          }
        }, children, this.closeButton()));
      }

      if (active) {
        return _react["default"].createElement("div", null, _react["default"].createElement("div", {
          className: classes,
          style: {
            background: color
          }
        }, children));
      }

      return _react["default"].createElement("div", null, _react["default"].createElement("div", {
        className: wasClosedClass,
        style: {
          background: color
        }
      }), this.openButton());
    }
  }]);

  return Message;
}(_react["default"].PureComponent);

exports["default"] = Message;

_defineProperty(Message, "propTypes", {
  children: _propTypes["default"].node.isRequired,
  active: _propTypes["default"].bool,
  fullScreen: _propTypes["default"].bool,
  closeType: _propTypes["default"].node,
  openType: _propTypes["default"].node,
  closePosition: _propTypes["default"].string,
  className: _propTypes["default"].string,
  color: _propTypes["default"].string
});

_defineProperty(Message, "defaultProps", {
  closeType: _react["default"].createElement(_icon["default"], {
    className: "close-default",
    type: "close-circle"
  }),
  closePosition: 'right',
  active: false,
  fullScreen: false,
  color: '#588DC8'
});