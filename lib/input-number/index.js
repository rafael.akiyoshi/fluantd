"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _inputNumber["default"];
  }
});

require("antd/lib/input-number/style");

var _inputNumber = _interopRequireDefault(require("antd/lib/input-number"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }