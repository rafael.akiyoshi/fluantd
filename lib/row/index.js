"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _row["default"];
  }
});

require("antd/lib/row/style");

var _row = _interopRequireDefault(require("antd/lib/row"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }