"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _cascader["default"];
  }
});

require("antd/lib/cascader/style");

var _cascader = _interopRequireDefault(require("antd/lib/cascader"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }