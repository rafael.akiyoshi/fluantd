"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _header = _interopRequireDefault(require("./header"));

var _body = _interopRequireDefault(require("./body"));

var _footer = _interopRequireDefault(require("./footer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var classPrefix = 'fluantd-responsive-drawer';

var ResponsiveDrawer = function ResponsiveDrawer(props) {
  var className = props.className,
      visible = props.visible,
      children = props.children;
  var classnames = (0, _classnames["default"])(classPrefix, className, {
    collapsed: !visible
  });
  return _react["default"].createElement("div", {
    className: classnames
  }, children);
};

ResponsiveDrawer.defaultProps = {
  visible: false
};
ResponsiveDrawer.Header = _header["default"];
ResponsiveDrawer.Body = _body["default"];
ResponsiveDrawer.Footer = _footer["default"];
var _default = ResponsiveDrawer;
exports["default"] = _default;