"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _audioWave["default"];
  }
});

var _audioWave = _interopRequireDefault(require("./audio-wave"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }