"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var bowser = require('bowser');

var wavesurfer;
var microphone;
var browserName = '';
var canUseDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

if (canUseDOM) {
  // eslint-disable-next-line global-require
  wavesurfer = require('wavesurfer.js'); // eslint-disable-next-line global-require

  microphone = require('wavesurfer.js/dist/plugin/wavesurfer.microphone');
  var browser = bowser.getParser(window.navigator.userAgent);
  browserName = browser.getBrowserName();
}

var AudioWave =
/*#__PURE__*/
function (_React$Component) {
  _inherits(AudioWave, _React$Component);

  function AudioWave(props) {
    var _this;

    _classCallCheck(this, AudioWave);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AudioWave).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "base64ToArrayBuffer", function (base64) {
      var binary_string = window.atob(base64);
      var len = binary_string.length;
      var bytes = new Uint8Array(len);

      for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
      }

      return bytes.buffer;
    });

    _defineProperty(_assertThisInitialized(_this), "getWaveSurfer", function () {
      var waveSurfer = _this.state.waveSurfer;
      return waveSurfer;
    });

    var _this$props = _this.props,
        audio = _this$props.audio,
        audio64 = _this$props.audio64;
    _this.state = {
      waveSurfer: {},
      audio: audio,
      audio64: audio64
    };
    return _this;
  }

  _createClass(AudioWave, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props2 = this.props,
          waveColor = _this$props2.waveColor,
          progressColor = _this$props2.progressColor,
          height = _this$props2.height,
          barWidth = _this$props2.barWidth,
          barHeight = _this$props2.barHeight,
          mode = _this$props2.mode,
          onRef = _this$props2.onRef,
          waveId = _this$props2.waveId,
          isNormalized = _this$props2.isNormalized;
      onRef(this);
      var _this$state = this.state,
          audio = _this$state.audio,
          audio64 = _this$state.audio64;
      var plugins = [];
      var interact = true;
      var audioContext = null;
      var audioScriptProcessor = null;
      var cursorWidth = 1;

      if (mode === 'record') {
        plugins.push(microphone.create());
        audio = '';
        audio64 = '';
        interact = false;

        if (browserName === 'Safari') {
          // Safari 11 or newer automatically suspends new AudioContext's that aren't
          // created in response to a user-gesture, like a click or tap, so create one
          // here (inc. the script processor)
          var AudioContext = window.AudioContext || window.webkitAudioContext;
          audioContext = new AudioContext();
          audioScriptProcessor = audioContext.createScriptProcessor(1024, 1, 1);
          cursorWidth = 0;
        }
      }

      var waveSurfer = wavesurfer.create({
        container: document.querySelector("#".concat(waveId)),
        waveColor: waveColor,
        progressColor: progressColor,
        height: height,
        barWidth: barWidth,
        barHeight: barHeight,
        responsive: true,
        isNormalized: isNormalized,
        interact: interact,
        cursorWidth: cursorWidth,
        audioScriptProcessor: audioScriptProcessor,
        audioContext: audioContext,
        plugins: plugins
      });

      if (audio) {
        waveSurfer.load(audio);
      } else if (audio64) {
        var bufferAudio = this.base64ToArrayBuffer(audio64);
        waveSurfer.loadArrayBuffer(bufferAudio);
      }

      this.setState({
        waveSurfer: waveSurfer
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var onRef = this.props.onRef;
      onRef(undefined);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          waveId = _this$props3.waveId,
          hide = _this$props3.hide,
          className = _this$props3.className;
      var classes = (0, _classnames["default"])(className);

      if (hide) {
        classes = (0, _classnames["default"])('hide-audio-wave', className);
      }

      return _react["default"].createElement("div", {
        id: waveId,
        className: classes
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      var base64ToArrayBuffer = function base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);

        for (var i = 0; i < len; i++) {
          bytes[i] = binary_string.charCodeAt(i);
        }

        return bytes.buffer;
      };

      var audio64 = state.audio64,
          audio = state.audio,
          waveSurfer = state.waveSurfer;

      if (props.audio64 !== audio64 || props.audio !== audio) {
        if (Object.keys(waveSurfer).length !== 0 && waveSurfer.constructor !== Object && props.audio64) {
          var buffer = base64ToArrayBuffer(props.audio64);
          waveSurfer.loadArrayBuffer(buffer);
        }

        return {
          audio64: props.audio64,
          audio: props.audio
        };
      }

      return null;
    }
  }]);

  return AudioWave;
}(_react["default"].Component);

AudioWave.propTypes = {
  waveId: _propTypes["default"].string,

  /**  Audio Path/link to play */
  audio: _propTypes["default"].string,
  audio64: _propTypes["default"].string,

  /** Audio wave visualizer color */
  waveColor: _propTypes["default"].string,

  /** Audio wave visualizer Progressed color */
  progressColor: _propTypes["default"].string,

  /** Audio wave div Height */
  height: _propTypes["default"].number,

  /** Audio wave visualizer bar width */
  barWidth: _propTypes["default"].number,

  /** Audio wave visualizer bar  hright */
  barHeight: _propTypes["default"].number,

  /** the wave mode maybe microphone  or player */
  mode: _propTypes["default"].string,

  /** Pass the waveSurfer API to parent */
  onRef: _propTypes["default"].func,

  /** hide the wave */
  hide: _propTypes["default"].bool,

  /** hide the wave */
  isNormalized: _propTypes["default"].bool,

  /** class */
  className: _propTypes["default"].string
};
AudioWave.defaultProps = {
  waveId: 'waveform',
  audio: '',
  audio64: '',
  waveColor: '#689bff',
  progressColor: '#364eff',
  height: 120,
  barWidth: 2,
  barHeight: 2,
  mode: 'player',
  hide: false,
  isNormalized: false
};
var _default = AudioWave;
exports["default"] = _default;