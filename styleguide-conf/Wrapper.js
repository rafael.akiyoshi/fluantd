import React, { Component } from 'react';
// import { IntlProvider } from 'react-intl';
import { Layout } from 'antd';

import './style.less';

export default class Wrapper extends Component {
  render() {
    return (
      <span > {this.props.children}</span>
    );
  }
}
