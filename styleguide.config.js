const path = require('path');
const { version } = require('./package.json');
const { styles, theme } = require('./styleguide.styles');

const lessToJs = require('less-vars-to-js');
const fs = require('fs');

const themeVariables = lessToJs(
  fs.readFileSync(path.join(__dirname, './theme/default.less'), 'utf8')
);

// lessToJs does not support @icon-url: "some-string", so we are manually
// adding it to the produced themeVariables js object here
themeVariables['@icon-url'] = "'http://localhost:8080/fonts/iconfont'";

module.exports = {
  title: `FluAntd ${version}`,
  editorConfig: { theme: 'cobalt' },
  template: {
    head:
      '<head>\n' +
      '    <meta charset="utf-8">\n' +
      '    <meta name="viewport" content="width=device-width">\n' +
      '    <link rel="apple-touch-icon" sizes="180x180" href="https://experience.everydayhero.do/apple-touch-icon.png">\n' +
      '    <link rel="icon" type="image/png" href="https://experience.everydayhero.do/favicon-32x32.png" sizes="32x32">\n' +
      '    <link rel="icon" type="image/png" href="https://experience.everydayhero.do/favicon-16x16.png" sizes="16x16">\n' +
      '    <link rel="manifest" href="https://experience.everydayhero.do/manifest.json">\n' +
      '    <link rel="mask-icon" href="https://experience.everydayhero.do/safari-pinned-tab.svg" color="#1bab6b">\n' +
      '    <meta name="apple-mobile-web-app-title" content="Everydayhero">\n' +
      '    <meta name="application-name" content="Everydayhero">\n' +
      '    <meta name="theme-color" content="#ffffff">\n' +
      '    <title><%= htmlWebpackPlugin.options.title %></title>\n' +
      '    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>\n' +
      '    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>\n' +
      '    <link rel="stylesheet" href="https://use.typekit.net/lbp2sai.css">\n' +
      '    <style type="text/css">\n' +
      '        /* Icons style overrides  */\n' +
      '        #Icon-container article { display: flex; flex-wrap: wrap; }\n' +
      '        #Icon-container article > div:first-child, #Icon-container article p { width: 100%; flex-basis: 100%; }\n' +
      "        #Icon-container article > div[class*='rsg--root'] { margin-right: 2rem; }\n" +
      "        #Icon-container article > div[class*='rsg--root'] [class*='rsg--preview'] { text-align: center; font-size: 1.5rem; }\n" +
      "        #Icon-container article > div[class*='rsg--root'] [class*='rsg--toolbar'] { display: none; }\n" +
      '    </style>\n' +
      '</head>'
  },
  styles,
  theme,
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'styleguide-conf/Wrapper')
  },
  ignore: ['components/**/index.js', 'components/**/*.test.js', 'components/**/setupTests.js'],
  sections: [
    {
      name: 'getting started',
      content: 'docs/getting-started.md'
    },
    {
      name: 'Folder structure',
      content: 'docs/folder-structure.md'
    },
    {
      name: 'conventional commits',
      content: 'docs/conventional-commits.md'
    },
    {
      name: 'style guide',
      content: 'docs/style-guide.md'
    },
    {
      name: 'Auto generated Change Log',
      content: 'CHANGELOG.md'
    },
    {
      name: 'components',
      components: 'components/**/*.js'
    }
  ],
  webpackConfig: {
    module: {
      rules: [
        // Babel loader, will use your project’s .babelrc
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        // Other loaders that are needed for your components
        {
          test: /\.css$/,
          loader: 'style-loader!css-loader?modules'
        },
        {
          test: /\.less$/,
          use: [
            { loader: 'style-loader' },
            { loader: 'css-loader' },
            {
              loader: 'less-loader',
              options: {
                javascriptEnabled: true,
                modifyVars: themeVariables
              }
            }
          ]
        },
        {
          test: /\.ttf$/,
          loader: 'url-loader', // or directly file-loader
          include: path.resolve(__dirname, 'node_modules/react-native-vector-icons')
        },
        {
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
          loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
        },
        {
          test: /\.(ttf|eot|woff|woff2)$/,
          use: {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]'
            }
          }
        }
      ]
    }
  },
  require: [
    path.join(__dirname, './theme/index.less'),
    path.join(__dirname, './theme/default.less')
  ]
};
