## Note

> **this Documentation is an extansion  for what exist in antd documentation. So all  [`antd's api`](https://ant.design/docs/react/introduce)  should works fine along side this api. if you are facing problem with it just report it** 
 
 
 ### install command  
```html
npm install
```
###  Test  
this will rin all jest test
```html
npm run test
```
if you don't want to run all test. you can specify  test file to run . 
###   todo  

###   start Documentation server  
```html
npm run docs
```
Documentation  is generated used react-styleguidist.  
- it will parse all components in   components   folder. 
- it will ignore index.js,  .test.js, setupTest.js  and less files. 
- it will parse all readme.md  files inside the components
- it will parse other additional  .md files in docs folder
- it will parse CHANGELOG.md

>   Note   :  any component's style example : components/button/style/index.less must be imported
in components/index.less to can be used by the documentation. unless   you wil miss all components style   

###   Build Documentation  

```html
num run build:docs
```
this will  build documentation in styleguide folder

###   generate change log  
```html
npm run changelog
```
it will generate CHANGELOG.md  that containes all changes from git commit message

>   Note  : Commit message MUST respect [conventional commits](https://conventionalcommits.org/).
a commitlint is implemented when commit action happen to enforce the conventions . read  [our documentation](#conventional-commits) for  conventional commits.

###  build library  
```html
npm run build:lib
```
it will build using babel  transforming to ecmascript5  and remove the readme.md and  .test.js
genrating a lib folder

###   build production  
```html
npm run build:prod
```
it will lint the code , run the tests ,build lib  , genrate the changelog and build documentation

###   git hooks 

git hooks using husky are command to execute when git command  will be executed

pre-commit  : it will lint all components folder
pre-push : it will run the tests
commit-msg :  commit lint message for commit conventions
 

