```html

-fluantd
  |
  -- components
  |   |
  |   -- index.js (import and export  index.js from <component-name folder>)
  |   |
  |   -- index.less (import  style/index.less from <component-name folder>)
  |   |
  |   -- setupTests.js ( test setup for jest)
  |   |
  |   -- <component-name folder> (example: button)
  |     |
  |     -- index.js
  |     |
  |     -- button.js
  |     |
  |     -- button.test.js (test jest)
  |     |
  |     -- Readme.md
  |     |
  |     -- style
  |       |
  |       -- index.less
  |
  -- docs (md documentatio file to be builded using styleguidist)
  |
  -- lib ( lib folder builded usin bable command)
  |
  -- styleguide (builded documentaion)
  |
  -- styleguide-conf (styleguidist config)
  |
  -- theme(contains global variable and style)

```

