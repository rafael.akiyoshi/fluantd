import React from 'react';
import { notification } from '../components';
import '../components/notification/style/index.less';

const container = {
  height: 400,
  padding: 100
};
const message = 'Notification Title';
const description =
  'This is the content of the notification. This is the content of the notification. This is the content of the notification.';

class Demo extends React.Component {
  state = {
    visible: true
  };

  notifSuccess = () => {
    const options = {
      type: 'success',
      message,
      description
    };
    notification(options);
  };

  notifWarning = () => {
    const options = {
      type: 'warning',
      message,
      description
    };
    notification(options);
  };

  notifInfo = () => {
    const options = {
      type: 'info',
      message,
      description
    };
    notification(options);
  };

  notifError = () => {
    const options = {
      type: 'error',
      message,
      description,
      duration: 0
    };
    notification(options);
  };

  render() {
    const onAction = () => {
      this.setState({ visible: false });
    };
    const onCancel = () => {
      this.setState({ visible: false });
    };
    const text = 'Are you sure you want to perform this action?';
    return (
      <div style={container}>
        <button onClick={this.notifWarning}>Alert</button>
        <button onClick={this.notifInfo}>Info</button>
        <button onClick={this.notifSuccess}>Success</button>
        <button onClick={this.notifError}>Error</button>
      </div>
    );
  }
}

export default Demo;
