import React from 'react';
import { render } from 'react-dom';
import '../theme/index.less';

import Demo from './demo';

render(
  <div className="bg-dark  container">
    <Demo />
  </div>,
  document.getElementById('app')
);
