import React from 'react';
import { shallow } from 'enzyme';
import Message from './message';
import Button from '../button/button';

describe('Sample Tests', () => {
  it('Should render normally', () => {
    const message = shallow(
      <Message>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.name()).toBe('div');
    expect(
      message
        .children()
        .children()
        .name()
    ).toBe('span');
    expect(
      message
        .children()
        .children()
        .children()
        .name()
    ).toBe('Icon');
  });

  it('Should change the Open Button', () => {
    const message = shallow(
      <Message openType={<Button>Open</Button>}>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(
      message
        .children()
        .children()
        .children()
        .name()
    ).toBe('Button');
  });

  it('Should open the Message component with default props', () => {
    const message = shallow(
      <Message>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.state('visible')).toBe(false);
    expect(
      message
        .children()
        .first()
        .props().className
    ).toBe('wasNotClosed-default');
    message.find('span').simulate('click');
    expect(message.state('visible')).toBe(true);
    expect(
      message
        .childAt(1)
        .first()
        .props().className
    ).toBe('fluant-message-default fluant-message-notActive');
  });

  it('Should close the Message component with default props', () => {
    const message = shallow(
      <Message>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.state('visible')).toBe(false);
    message.find('span').simulate('click');
    expect(message.state('visible')).toBe(true);
    message.find('.close-right').simulate('click');
    expect(message.state('visible')).toBe(false);
  });

  it('Should accept another open button', () => {
    const message = shallow(
      <Message openType={<Button className="inner-button">Open</Button>}>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.state('visible')).toBe(false);
    message
      .find('.inner-button')
      .parent()
      .simulate('click');
    expect(message.state('visible')).toBe(true);
  });

  it('Should accept another close button', () => {
    const message = shallow(
      <Message closeType={<Button className="inner-button">Close</Button>}>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.state('visible')).toBe(false);
    message.find('span').simulate('click');
    expect(message.state('visible')).toBe(true);
    message
      .find('.inner-button')
      .parent()
      .simulate('click');
    expect(message.state('visible')).toBe(false);
  });

  it('Should be able to change the close button position', () => {
    const messageOne = shallow(
      <Message closeType={<Button className="inner-button">Close</Button>} closePosition="center">
        <h2>Inner Component</h2>
      </Message>
    );
    messageOne.find('span').simulate('click');
    expect(
      messageOne
        .find('.inner-button')
        .parent()
        .props().className
    ).toBe('close-center');

    const messageTwo = shallow(
      <Message closeType={<Button className="inner-button">Close</Button>} closePosition="left">
        <h2>Inner Component</h2>
      </Message>
    );
    messageTwo.find('span').simulate('click');
    expect(
      messageTwo
        .find('.inner-button')
        .parent()
        .props().className
    ).toBe('close-left');
  });

  it('Should validate the inner components', () => {
    const message = shallow(
      <Message>
        <h2 className=".inner-component">Inner Component</h2>
      </Message>
    );
    expect(message.contains(<h2 className=".inner-component">Inner Component</h2>)).toBe(false);
    message.find('span').simulate('click');
    expect(message.contains(<h2 className=".inner-component">Inner Component</h2>)).toBe(true);
    message.find('.close-right').simulate('click');
    expect(message.contains(<h2 className=".inner-component">Inner Component</h2>)).toBe(false);
  });

  it('Should be able to click in a inner button component', () => {
    let buttonClass = 'initial';
    const message = shallow(
      <Message>
        <Button onClick={(buttonClass = 'final')} className="innerButton">
          InnerButton
        </Button>
      </Message>
    );
    message.find('span').simulate('click');
    message
      .find('.innerButton')
      .parent()
      .simulate('click');
    expect(buttonClass).toBe('final');
  });

  it('Should accept custom classNames for the Message Open button', () => {
    const message = shallow(
      <Message className="custom-className">
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.state('visible')).toBe(false);
    message.find('span').simulate('click');
    expect(message.state('visible')).toBe(true);
  });

  it('Should be able to change the Component overlays color', () => {
    const message = shallow(
      <Message color="red">
        <h2>Inner Component</h2>
      </Message>
    );
    expect(
      message
        .children()
        .first()
        .prop('style')
    ).toEqual({ background: 'red' });
  });

  it('Should start visible when active', () => {
    const message = shallow(
      <Message active>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.children().prop('className')).toMatch(/active/);
  });

  it('Should not be closable when active', () => {
    const message = shallow(
      <Message active>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.children().prop('className')).toMatch(/active/);
    expect(message.find('.close-right').exists()).toBe(false);
  });

  it('Should be able to put it fullScreen', () => {
    const message = shallow(
      <Message fullScreen>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.state('visible')).toBe(false);
    message.find('span').simulate('click');
    expect(message.state('visible')).toBe(true);
    expect(message.childAt(1).prop('className')).toMatch(/fullScreen/);
    message.find('.close-right').simulate('click');
    expect(message.state('visible')).toBe(false);
  });

  it('Should be able to put it fullScreen and active', () => {
    const message = shallow(
      <Message fullScreen active>
        <h2>Inner Component</h2>
      </Message>
    );
    expect(message.children().prop('className')).toBe(
      'fluant-message-fullScreen fluant-message-active'
    );
    expect(message.find('.close-right').exists()).toBe(false);
  });
});
