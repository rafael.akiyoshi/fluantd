import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'antd/lib/icon';

import classNames from 'classnames';

export default class Message extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    active: PropTypes.bool,
    fullScreen: PropTypes.bool,
    closeType: PropTypes.node,
    openType: PropTypes.node,
    closePosition: PropTypes.string,
    className: PropTypes.string,
    color: PropTypes.string
  };

  static defaultProps = {
    closeType: <Icon className="close-default" type="close-circle" />,
    closePosition: 'right',
    active: false,
    fullScreen: false,
    color: '#588DC8'
  };

  state = {
    visible: false,
    wasClosed: false
  };

  openMessage = () => {
    this.setState({ visible: true });
  };

  closeMessage = () => {
    this.setState({ visible: false });
    this.setState({ wasClosed: true });
  };

  closeButton = () => {
    const { closeType, closePosition, active } = this.props;
    if (!active) {
      return (
        <span
          className={`close-${closePosition}`}
          onClick={this.closeMessage}
          role="presentation"
          onKeyDown={this.closeMessage}
        >
          {closeType}
        </span>
      );
    }
    return null;
  };

  openButton = () => {
    const { openType, className, active, color } = this.props;
    const { visible } = this.state;
    const openTypeClass = openType ? 'byProps' : 'default';
    const visibleClass = visible ? 'visible' : 'invisible';
    const classes = classNames(className, {
      [`open-${openTypeClass}-${visibleClass}`]: openTypeClass
    });
    if (!active) {
      if (openType) {
        return (
          <div className={classes}>
            <span onClick={this.openMessage} role="presentation" onKeyDown={this.openMessage}>
              {openType}
            </span>
          </div>
        );
      }
      return (
        <div className={className}>
          <span role="presentation" onClick={this.openMessage} onKeyDown={this.openMessage}>
            <Icon type="question-circle" style={{ background: color }} className={classes} />
          </span>
        </div>
      );
    }
    return null;
  };

  render() {
    const { children, active, fullScreen, color } = this.props;
    const { visible, wasClosed } = this.state;
    const fullScreenClass = fullScreen ? 'fullScreen' : 'default';
    const activeClass = active ? 'active' : 'notActive';
    const wasClosedClass = wasClosed
      ? `wasClosed-${fullScreenClass}`
      : `wasNotClosed-${fullScreenClass}`;
    const classes = classNames({
      [`fluant-message-${fullScreenClass}`]: fullScreenClass,
      [`fluant-message-${activeClass}`]: activeClass
    });
    if (visible) {
      return (
        <div>
          {this.openButton()}
          <div className={classes} style={{ background: color }}>
            {children}
            {this.closeButton()}
          </div>
        </div>
      );
    }
    if (active) {
      return (
        <div>
          <div className={classes} style={{ background: color }}>
            {children}
          </div>
        </div>
      );
    }
    return (
      <div>
        <div className={wasClosedClass} style={{ background: color }} />
        {this.openButton()}
      </div>
    );
  }
}
