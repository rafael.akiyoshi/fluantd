import React from 'react';
import AntCard from 'antd/lib/card';
import 'antd/lib/card/style';
import classNames from 'classnames';
import Header from './header';
import Body from './body';
import Footer from './footer';

const classPrefix = 'fluantd-card';
const Card = props => {
  const { background, className, ...restProps } = props;
  const classnames = classNames(classPrefix, className);
  return <AntCard {...restProps} className={classnames} />;
};

const { Meta, Grid } = Card;

Card.Meta = Meta;
Card.Grid = Grid;
Card.Header = Header;
Card.Body = Body;
Card.Footer = Footer;

export default Card;
