import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Card from './card';

const { Header, Body, Footer } = Card;
const FIRST_CHILD = 0;

describe('Simple Tests', () => {
  it('Testing sample instance, if Fluant render Ant components through proxy', () => {
    const fCard = shallow(<Card />);
    expect(fCard.getElement(FIRST_CHILD).type.name).toBe('Card');
    expect(fCard.getElement(FIRST_CHILD).props.className).toBe('fluantd-card');
  });

  it('Testing sample instance with header', () => {
    const fCard = shallow(
      <Card>
        <Header>
          <h2>This is a header</h2>
        </Header>
      </Card>
    );
    expect(fCard.children().name()).toBe('Header');
    expect(
      fCard
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with footer', () => {
    const fCard = shallow(
      <Card>
        <Footer>
          <h2>This is a Footer</h2>
        </Footer>
      </Card>
    );
    expect(fCard.children().name()).toBe('Footer');
    expect(
      fCard
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with Body', () => {
    const fCard = shallow(
      <Card>
        <Body>
          <h2>This is a Body</h2>
        </Body>
      </Card>
    );
    expect(fCard.children().name()).toBe('Body');
    expect(
      fCard
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with everything', () => {
    const fCard = shallow(
      <Card>
        <Header>
          <h2>This is a header</h2>
        </Header>
        <Body>
          <h2>This is a Body</h2>
        </Body>
        <Footer>
          <h2>This is a Footer</h2>
        </Footer>
      </Card>
    );
    const expectComponents = ['Header', 'Body', 'Footer'];
    fCard.children().forEach(node => {
      expect(expectComponents.includes(node.name())).toBe(true);
    });
  });

  it('Testing proxying props ', () => {
    const fCard = shallow(
      <Card hoverable bordered={false}>
        <Header>
          <h2>This is a Header</h2>
        </Header>
      </Card>
    );
    expect(fCard.getElement(FIRST_CHILD).props.hoverable).toBe(true);
    expect(fCard.getElement(FIRST_CHILD).props.bordered).toBe(false);
  });
});

describe('SnapShots tests', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <Card>
          <Header>
            <h1>This is a Header</h1>
          </Header>
          <Body>
            <h2>This is a Body</h2>
          </Body>
          <Footer>
            <h3>This is a Footer</h3>
          </Footer>
        </Card>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Testing proxying props', () => {
    const tree = renderer
      .create(
        <Card hoverable bordered={false} style={{ width: 300, borderRadius: 10 }}>
          <Header>
            <h1>This is a Header</h1>
          </Header>
          <Body>
            <h2>This is a Body</h2>
          </Body>
          <Footer>
            <h3>This is a Footer</h3>
          </Footer>
        </Card>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
