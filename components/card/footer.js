import React from 'react';
import classNames from 'classnames';

const classPrefix = 'fluantd-card-footer';
const Footer = props => {
  const { className, ...restProps } = props;
  const classnames = classNames(classPrefix, className);
  return <div {...restProps} className={classnames} />;
};

export default Footer;
