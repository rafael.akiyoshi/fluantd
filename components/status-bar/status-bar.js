import React from 'react';
import Tooltip from 'antd/lib/tooltip';

import classNames from 'classnames';
import PropTypes from 'prop-types';

/**
 * StatusBar Component is used for explicit inform the
 * user about the situation of something, using colors.
 */
const StatusBar = ({
  info,
  placement,
  hoverable,
  color,
  width,
  height,
  status,
  borderRadius,
  className,
  ...props
}) => {
  if (hoverable) {
    hoverable = 'hoverable-';
  } else {
    hoverable = '';
  }
  const classes = classNames(className, {
    [`fluantd-${hoverable}${status}`]: status
  });

  const StatusBarStyle = {
    width,
    background: color,
    height,
    borderRadius
  };

  return (
    <Tooltip placement={placement} title={info} arrowPointAtCenter>
      <div className={classes} style={StatusBarStyle} {...props} />
    </Tooltip>
  );
};

StatusBar.propTypes = {
  info: PropTypes.string,
  placement: PropTypes.string,
  hoverable: PropTypes.bool,
  color: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.number,
  status: PropTypes.string,
  borderRadius: PropTypes.string,
  className: PropTypes.string
};

StatusBar.defaultProps = {
  info: '',
  placement: 'top',
  hoverable: false,
  width: '100%',
  height: 8,
  status: 'primary',
  borderRadius: '100px'
};

export default StatusBar;
