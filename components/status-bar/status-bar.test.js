import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import StatusBar from './status-bar';

const FIRST_CHILD = 0;

describe('Simple Tests', () => {
  it('Should render normally', () => {
    const statusBar = shallow(<StatusBar />);
    expect(statusBar.getElement(FIRST_CHILD).type.name).toBe('Tooltip');
    expect(statusBar.children().getElement(FIRST_CHILD).type).toBe('div');
  });

  it('Should receive the default props', () => {
    const statusBar = shallow(<StatusBar />);
    const { width, height, borderRadius } = statusBar.getElement(
      FIRST_CHILD
    ).props.children.props.style;
    const { placement, title } = statusBar.getElement(FIRST_CHILD).props;
    expect(width).toBe('100%');
    expect(height).toBe(8);
    expect(borderRadius).toBe('100px');
    expect(placement).toBe('top');
    expect(title).toBe('');
  });

  it('Should change the props', () => {
    const statusBar = shallow(
      <StatusBar
        height={20}
        width="50%"
        info="some tooltip"
        placement="topLeft"
        borderRadius="20px"
      />
    );
    const { width, height, borderRadius } = statusBar.getElement(
      FIRST_CHILD
    ).props.children.props.style;
    const { placement, title } = statusBar.getElement(FIRST_CHILD).props;
    expect(width).toBe('50%');
    expect(height).toBe(20);
    expect(borderRadius).toBe('20px');
    expect(placement).toBe('topLeft');
    expect(title).toBe('some tooltip');
  });

  it('Should change the class when hoverable changed', () => {
    const statusBar = shallow(<StatusBar hoverable />);
    expect(statusBar.getElement(FIRST_CHILD).props.children.props.className).toContain('hoverable');
    statusBar.setProps({ hoverable: false });
    expect(statusBar.getElement(FIRST_CHILD).props.children.props.className).not.toContain(
      'hoverable'
    );

    const statusBarError = shallow(<StatusBar hoverable status="error" />);
    expect(statusBarError.getElement(FIRST_CHILD).props.children.props.className).toContain(
      'hoverable'
    );
    statusBarError.setProps({ hoverable: false });
    expect(statusBarError.getElement(FIRST_CHILD).props.children.props.className).not.toContain(
      'hoverable'
    );
  });

  it('Should accept and render inner components', () => {
    const statusBar = shallow(
      <StatusBar>
        <h2>This is an inner component</h2>
      </StatusBar>
    );
    expect(statusBar.children().text()).toBe('This is an inner component');
    expect(statusBar.children().contains(<h2>This is an inner component</h2>)).toBe(true);
  });
});

describe('SnapShots Tests', () => {
  it('Should render normally', () => {
    const tree = renderer.create(<StatusBar />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should recive the default props', () => {
    const tree = renderer.create(<StatusBar />).toJSON();
    expect(tree.props.style).toMatchSnapshot({
      borderRadius: '100px',
      height: 8,
      width: '100%'
    });
  });

  it('Should change the props', () => {
    const tree = renderer
      .create(
        <StatusBar
          height={20}
          width="50%"
          info="some tooltip"
          placement="topLeft"
          borderRadius="20px"
        />
      )
      .toJSON();
    expect(tree.props.style).toMatchSnapshot({
      borderRadius: '20px',
      height: 20,
      width: '50%'
    });
  });

  it('Should change the class when is hoverable', () => {
    const treeWithoutHoverable = renderer.create(<StatusBar />).toJSON();
    expect(treeWithoutHoverable.props.className).toMatchSnapshot('fluantd-primary');

    const treeHoverable = renderer.create(<StatusBar />).toJSON();
    expect(treeHoverable.props.className).toMatchSnapshot('fluantd-hoverable-primary');
  });

  it('Should accept and render inner components', () => {
    const tree = renderer
      .create(
        <StatusBar>
          <h2>This is an inner component</h2>
        </StatusBar>
      )
      .toJSON();
    expect(tree.children[0].type).toBe('h2');
    expect(tree).toMatchSnapshot();
  });
});
