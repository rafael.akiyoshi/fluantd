import React from 'react';
import Icon from 'antd/lib/icon';
import classNames from 'classnames';
import PropTypes from 'prop-types';

class PlayPauseButton extends React.PureComponent {
  constructor(props) {
    super(props);
    const { status } = this.props;

    this.state = {
      statusBoolean: status === 'play' ? 1 : 0
    };
  }

  playPause = () => {
    const { statusBoolean } = this.state;
    this.setState({ statusBoolean: !statusBoolean });
  };

  render() {
    const { size, color, disabled, className, ...rest } = this.props;

    const styles = {
      fontSize: `${size}px`,
      color
    };

    const { statusBoolean } = this.state;
    const classes = classNames('play-pause', className);

    return (
      <a onMouseUp={this.playPause} role="button" disabled={disabled} className={classes} {...rest}>
        {statusBoolean ? (
          <Icon style={styles} type="caret-right" />
        ) : (
          <Icon style={styles} type="pause" />
        )}
      </a>
    );
  }
}

PlayPauseButton.propTypes = {
  status: PropTypes.string,
  size: PropTypes.number,
  color: PropTypes.string,
  disabled: PropTypes.bool
};

PlayPauseButton.defaultProps = {
  size: 70,
  color: 'white',
  disabled: false,
  status: 'play'
};

export default PlayPauseButton;
