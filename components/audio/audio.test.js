import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import PlayPauseButton from './play-pause-button';
import RecordButton from './record-button';

describe('Tests RecordButton ', () => {
  test('should be rendred', () => {
    const props = { disabled: true };
    const recordBtn = renderer.create(<RecordButton id="testDisabled" {...props} />).toJSON();
    expect(recordBtn).toMatchSnapshot();
  });
  test('should be dsiabled', () => {
    const props = { disabled: true };
    const recordBtn = renderer.create(<RecordButton id="testRender" {...props} />).toJSON();
    expect(recordBtn.props.disabled).toMatchSnapshot();
  });

  test('should be resized button', () => {
    const props = { buttonSize: 70 };
    const recordBtn = renderer.create(<RecordButton id="testBtnSize" {...props} />).toJSON();
    expect(recordBtn.props.style).toMatchSnapshot({
      height: '70px',
      width: '70px'
    });
  });

  test('should be resized Icon', () => {
    const props = { iconSize: 20 };
    const recordBtn = renderer.create(<RecordButton id="testIconSize" {...props} />).toJSON();
    expect(recordBtn.children[0].props.style).toMatchSnapshot({
      height: '20px',
      width: '20px'
    });
  });
});

describe('Tests PlayPauseButton ', () => {
  test('should be rendred', () => {
    const props = { disabled: true };
    const PPBtn = renderer.create(<PlayPauseButton id="testRender" {...props} />).toJSON();
    expect(PPBtn).toMatchSnapshot();
  });

  test('should be disabled', () => {
    const props = { disabled: true };
    const PPBtn = renderer.create(<PlayPauseButton id="testDisabled" {...props} />).toJSON();
    expect(PPBtn.props.disabled).toMatchSnapshot();
  });

  test('should be resized icon', () => {
    const props = { size: 80 };
    const PPBtn = renderer.create(<PlayPauseButton id="testSize" {...props} />).toJSON();
    expect(PPBtn.children[0].props.style).toMatchSnapshot({
      fontSize: '80px',
      color: 'white'
    });
  });

  test('should be color icon', () => {
    const props = { color: 'red' };
    const PPBtn = renderer.create(<PlayPauseButton id="testColor" {...props} />).toJSON();
    expect(PPBtn.children[0].props.style).toMatchSnapshot({
      fontSize: '70px',
      color: 'red'
    });
  });

  test('should be status pause', () => {
    const props = { status: 'pause' };
    const PPBtn = renderer.create(<PlayPauseButton id="testPause" {...props} />).toJSON();
    expect(PPBtn.children[0].props.className).toBe('anticon anticon-pause');
  });

  test('should be status play', () => {
    const props = { status: 'play' };
    const PPBtn = renderer.create(<PlayPauseButton id="testPlay" {...props} />).toJSON();
    expect(PPBtn.children[0].props.className).toBe('anticon anticon-caret-right');
  });

  it('should when clock change  status  to play', () => {
    const props = { status: 'pause' };
    let PPBtn = shallow(<PlayPauseButton id="testBtn" {...props} />);
    expect(
      PPBtn.find('a')
        .children()
        .props().type
    ).toEqual('pause');
    PPBtn = PPBtn.simulate('mouseup');
    expect(
      PPBtn.find('a')
        .children()
        .props().type
    ).toEqual('caret-right');
  });

  it('should when clock change  status  to pause', () => {
    const props = { status: 'play' };
    let PPBtn = shallow(<PlayPauseButton id="testBtn" {...props} />);
    expect(
      PPBtn.find('a')
        .children()
        .props().type
    ).toEqual('caret-right');
    PPBtn = PPBtn.simulate('mouseup');
    expect(
      PPBtn.find('a')
        .children()
        .props().type
    ).toEqual('pause');
  });
});
