import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Button from '../button';

const RecordButton = ({ buttonSize, iconSize, disabled, className, ...props }) => {
  const buttonStyle = {
    height: `${buttonSize}px`,
    width: `${buttonSize}px`
  };

  const iconStyle = {
    height: `${iconSize}px`,
    width: `${iconSize}px`
  };

  const classes = classNames('record', className);

  return (
    <Button style={buttonStyle} shape="circle" disabled={disabled} className={classes} {...props}>
      <img style={iconStyle} src="../../theme/icons/recordings.svg" alt="" />
    </Button>
  );
};

RecordButton.propTypes = {
  buttonSize: PropTypes.number,
  iconSize: PropTypes.number,
  disabled: PropTypes.bool
};

RecordButton.defaultProps = {
  buttonSize: 63,
  iconSize: 43,
  disabled: false
};

export default RecordButton;
