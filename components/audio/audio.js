import React from 'react';
import PropTypes from 'prop-types';
import Card from '../card';

import PlayPauseButton from './play-pause-button';
import RecordButton from './record-button';

const Audio = ({ children, ...props }) => {
  return (
    <Card className="audio-card" {...props}>
      {children}
    </Card>
  );
};

Audio.protoType = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

Audio.Header = Card.Header;
Audio.Footer = Card.Footer;
Audio.Body = Card.Body;
Audio.RecordButton = RecordButton;
Audio.PlayPauseButton = PlayPauseButton;

export default Audio;
