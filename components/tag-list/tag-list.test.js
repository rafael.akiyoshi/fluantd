import React from 'react';
import { shallow } from 'enzyme';
import TagList from './tag-list';

const FIRST_CHILD = 0;

describe('Sample tests', () => {
  it('Should render normally', () => {
    const tagList = shallow(<TagList listOfTags={['tag1', 'tag2', 'tag3']} />);
    expect(tagList.children().name()).toBe('Slider');
    tagList
      .children()
      .children()
      .forEach(node => {
        expect(node.name()).toBe('Tag');
      });
  });

  it('Should receive the default props', () => {
    const tagList = shallow(<TagList listOfTags={['tag1', 'tag2', 'tag3']} />);
    const { infinite, slidesToShow, slidesToScroll, autoPlay, autoPlaySpeed } = tagList.getElement(
      FIRST_CHILD
    ).props.children.props;
    expect(infinite).toBe(false);
    expect(slidesToShow).toBe(6);
    expect(slidesToScroll).toBe(4);
    expect(autoPlay).toBe(false);
    expect(autoPlaySpeed).toBe(0);
  });

  it('Should change the props', () => {
    const tagList = shallow(
      <TagList
        listOfTags={['tag1', 'tag2', 'tag3']}
        autoPlay
        infinite
        slidesToShow={8}
        slidesToScroll={2}
        autoPlaySpeed={500}
      />
    );
    const { infinite, slidesToShow, slidesToScroll, autoPlay, autoPlaySpeed } = tagList.getElement(
      FIRST_CHILD
    ).props.children.props;
    expect(infinite).toBe(true);
    expect(slidesToShow).toBe(8);
    expect(slidesToScroll).toBe(2);
    expect(autoPlay).toBe(true);
    expect(autoPlaySpeed).toBe(500);
  });

  it('Should show tags that has been passed', () => {
    const tags = ['#tag1', '#tag2', '#tag3'];
    const tagList = shallow(<TagList listOfTags={tags} />);
    tagList
      .children()
      .children()
      .forEach((node, index) => {
        expect(node.name()).toBe('Tag');
        expect(node.getElement(FIRST_CHILD).key).toBe(tags[index]);
      });
  });
});
