import React from 'react';
import Slide from 'react-slick';
import PropTypes from 'prop-types';
import Tag from 'antd/lib/tag';

const TagList = ({
  slidesToScroll,
  slidesToShow,
  infinite,
  autoPlay,
  autoPlaySpeed,
  listOfTags,
  ...props
}) => {
  const settings = {
    infinite,
    slidesToShow,
    slidesToScroll,
    autoPlay,
    autoPlaySpeed,
    responsive: [
      {
        breakpoint: 968,
        settings: {
          slidesToShow: slidesToShow - 1,
          slidesToScroll: slidesToScroll - 1
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: slidesToShow - 2,
          slidesToScroll: slidesToScroll - 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: slidesToShow - 3,
          slidesToScroll: slidesToScroll - 3
        }
      }
    ]
  };
  return (
    <div>
      <Slide className="slider-container" {...settings}>
        {listOfTags.map(itemList => {
          return (
            <Tag {...props} key={itemList} className="tags-container">
              {itemList}
            </Tag>
          );
        })}
      </Slide>
    </div>
  );
};

TagList.propTypes = {
  slidesToScroll: PropTypes.number,
  slidesToShow: PropTypes.number,
  infinite: PropTypes.bool,
  autoPlay: PropTypes.bool,
  autoPlaySpeed: PropTypes.number,
  listOfTags: PropTypes.array.isRequired
};

TagList.defaultProps = {
  infinite: false,
  slidesToShow: 6,
  slidesToScroll: 4,
  autoPlay: false,
  autoPlaySpeed: 0
};

export default TagList;
