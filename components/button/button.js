import React from 'react';
import AntButton from 'antd/lib/button';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import 'antd/lib/button/style';

const Button = ({ type, shape, className, ...props }) => {
  const classes = classNames(className, {
    [`ant-btn-${type}`]: type,
    [`ant-btn-${shape}`]: shape
  });

  return <AntButton {...props} className={classes} />;
};

Button.propTypes = {
  /** the type could be one of  : primary , danger , alert,  success */
  type: PropTypes.string,
  /** the shape could be one of  : rounded , rounded-outline */
  shape: PropTypes.string,
  ghost: PropTypes.bool,
  disabled: PropTypes.bool
};
Button.Group = AntButton.Group;
export default Button;
