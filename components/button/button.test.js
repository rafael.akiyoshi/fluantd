import React from 'react';
import { shallow, configure } from 'enzyme';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import Button from './button';

configure({ adapter: new Adapter() });

describe('Tests for colorful buttons', () => {
  it('Button With Primary style', () => {
    const props = { type: 'primary' };
    const fButton = shallow(
      <Button id="test" {...props}>
        Off
      </Button>
    );
    expect(fButton.hasClass('ant-btn-primary')).toEqual(true);
  });

  it('Button With Success style', () => {
    const props = { type: 'success' };
    const fButton = shallow(
      <Button id="test" {...props}>
        Off
      </Button>
    );
    expect(fButton.hasClass('ant-btn-success')).toEqual(true);
  });

  it('Button With Danger style', () => {
    const props = { type: 'danger' };
    const fButton = shallow(
      <Button id="test" {...props}>
        Off
      </Button>
    );
    expect(fButton.hasClass('ant-btn-danger')).toEqual(true);
  });

  it('Button With Alert style', () => {
    const props = { type: 'alert' };
    const fButton = shallow(
      <Button id="test" {...props}>
        Off
      </Button>
    );
    expect(fButton.hasClass('ant-btn-alert')).toEqual(true);
  });
});

describe('Tests for Ghost buttons', () => {
  it('Button With Primary style', () => {
    const props = {
      type: 'primary',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-primary');
  });

  it('Button With Success style', () => {
    const props = {
      type: 'success',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-success');
  });

  it('Button With Default style', () => {
    const props = {
      type: 'danger',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-danger');
  });

  it('Button With alert style', () => {
    const props = {
      type: 'alert',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-alert');
  });
});

describe('Tests for Disabled buttons', () => {
  it('Button With Primary style', () => {
    const props = {
      type: 'primary',
      disabled: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('disabled')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-primary');
  });

  it('Button With Success style', () => {
    const props = {
      type: 'success',
      disabled: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('disabled')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-success');
  });

  it('Button With Default style', () => {
    const props = {
      type: 'danger',
      disabled: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('disabled')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-danger');
  });

  it('Button With alert style', () => {
    const props = {
      type: 'alert',
      disabled: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('disabled')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-alert');
  });
});

describe('Tests for Rounded buttons', () => {
  it('Button With Primary style', () => {
    const props = {
      type: 'primary',
      shape: 'rounded'
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('className')).toBe('ant-btn-primary ant-btn-rounded');
  });

  it('Button With Success style', () => {
    const props = {
      type: 'success',
      shape: 'rounded'
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('className')).toBe('ant-btn-success ant-btn-rounded');
  });
  it('Button With Danger style', () => {
    const props = {
      type: 'danger',
      shape: 'rounded'
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('className')).toBe('ant-btn-danger ant-btn-rounded');
  });
  it('Button With Alert style', () => {
    const props = {
      type: 'alert',
      shape: 'rounded'
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('className')).toBe('ant-btn-alert ant-btn-rounded');
  });
});

describe('Tests for Rounded Ghost buttons', () => {
  it('Button With Primary style', () => {
    const props = {
      type: 'primary',
      shape: 'rounded',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-primary ant-btn-rounded');
  });
  it('Button With Success style', () => {
    const props = {
      type: 'success',
      shape: 'rounded',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-success ant-btn-rounded');
  });
  it('Button With Danger style', () => {
    const props = {
      type: 'danger',
      shape: 'rounded',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-danger ant-btn-rounded');
  });
  it('Button With Alert style', () => {
    const props = {
      type: 'alert',
      shape: 'rounded',
      ghost: true
    };
    const fButton = shallow(<Button {...props} />);
    expect(fButton.prop('ghost')).toBe(true);
    expect(fButton.prop('className')).toBe('ant-btn-alert ant-btn-rounded');
  });
});

describe('Tests for SNAPSHOTS buttons', () => {
  test('Button With Default style', () => {
    const component = renderer.create(<Button>Some Button</Button>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Primary style', () => {
    const component = renderer.create(<Button type="primary">Some Button</Button>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Danger style', () => {
    const component = renderer.create(<Button type="danger">Some Button</Button>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Alert style', () => {
    const component = renderer.create(<Button type="alert">Some Button</Button>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Success style', () => {
    const component = renderer.create(<Button type="success">Some Button</Button>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Primary and ghost style', () => {
    const component = renderer.create(
      <Button ghost type="primary">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Danger and ghost style', () => {
    const component = renderer.create(
      <Button ghost type="danger">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Alert and ghost style', () => {
    const component = renderer.create(
      <Button ghost type="alert">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Success and ghost style', () => {
    const component = renderer.create(
      <Button ghost type="success">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Primary and rounded style', () => {
    const component = renderer.create(
      <Button shape="rounded" type="primary">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Danger and rounded style', () => {
    const component = renderer.create(
      <Button shape="rounded" type="danger">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Alert and rounded style', () => {
    const component = renderer.create(
      <Button shape="rounded" type="alert">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('Button With Success and rounded style', () => {
    const component = renderer.create(
      <Button shape="rounded" type="success">
        Some Button
      </Button>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
