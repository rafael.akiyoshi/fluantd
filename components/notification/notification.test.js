// import renderer from 'react-test-renderer';
import notification from './notification';

const message = 'simple message';
const description = 'simple description';
function $$(className) {
  return document.body.querySelectorAll(className);
}
describe('Testing different notification types', () => {
  test('Rendering info notification', () => {
    const options = {
      type: 'info',
      message,
      description
    };
    notification(options);
    expect($$('.notice-info').length).toBeGreaterThan(0);
  });
  test('Rendering success notification', () => {
    const options = {
      type: 'success',
      message,
      description
    };
    notification(options);
    expect($$('.notice-success').length).toBeGreaterThan(0);
  });
  test('Rendering alert notification', () => {
    const options = {
      type: 'warning',
      message,
      description
    };
    notification(options);
    expect($$('.notice-warning').length).toBeGreaterThan(0);
  });
  test('Rendering error notification', () => {
    const options = {
      type: 'error',
      message,
      description
    };
    notification(options);
    expect($$('.notice-error').length).toBeGreaterThan(0);
  });
});
