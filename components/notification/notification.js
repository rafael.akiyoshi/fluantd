import notif from 'antd/lib/notification';
import './style/index.less';
import 'antd/lib/notification/style';

const notification = props => {
  const { type, ...restProps } = props;
  notif.open({
    className: `notice-${type}`,
    type,
    ...restProps
  });
};

export default notification;
