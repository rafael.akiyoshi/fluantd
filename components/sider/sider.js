import React from 'react';
import Layout from 'antd/lib/layout';

import classNames from 'classnames';
import PropTypes from 'prop-types';

const AntSider = Layout.Sider;

/**
 * This component is a Sider for a Sider Menu Layout.
 * You still have to use Layout and Menu components in order to use it.
 */

const Sider = ({ theme, ...props }) => {
  const classes = classNames({
    [`${theme}-grad`]: theme
  });
  return <AntSider {...props} className={classes} />;
};

Sider.propTypes = {
  theme: PropTypes.string
};

export default Sider;
