import React from 'react';
import { shallow, configure } from 'enzyme';
import renderer from 'react-test-renderer';
import Adapter from 'enzyme-adapter-react-16';
import Sider from './sider';

configure({ adapter: new Adapter() });

describe('Simple Tests', () => {
  it('Testing sample instance', () => {
    const fSideBar = shallow(<Sider />);
    expect(fSideBar.get(0).type.name).toBe('Sider');
  });

  it('Should pass the correct className as primary theme', () => {
    const fSideBar = shallow(<Sider theme="primary" />);
    expect(fSideBar.props().className).toBe('primary-grad');
  });

  it('Should pass the correct className as secondary theme', () => {
    const fSideBar = shallow(<Sider theme="secondary" />);
    expect(fSideBar.props().className).toBe('secondary-grad');
  });

  it('Should pass the correct className as tertiary theme', () => {
    const fSideBar = shallow(<Sider theme="tertiary" />);
    expect(fSideBar.props().className).toBe('tertiary-grad');
  });

  it('Testing changing Sider props', () => {
    const fSideBar1 = shallow(<Sider />);
    expect(fSideBar1.get(0).props.collapsible).toBe(false);

    const fSideBar2 = shallow(<Sider collapsible />);
    expect(fSideBar2.get(0).props.collapsible).toBe(true);

    const fSideBar3 = shallow(<Sider collapsedWidth={90} />);
    expect(fSideBar3.get(0).props.collapsedWidth).toBe(90);

    const fSideBar4 = shallow(<Sider reverseArrow />);
    expect(fSideBar4.get(0).props.reverseArrow).toBe(true);

    const fSideBar5 = shallow(<Sider reverseArrow />);
    expect(fSideBar5.get(0).props.reverseArrow).toBe(true);
  });
});

describe('SnapShots Tests', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<Sider collapsible />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Passing themes', () => {
    const treePrimary = renderer.create(<Sider theme="primary" />).toJSON();
    expect(treePrimary).toMatchSnapshot();

    const treeSecondary = renderer.create(<Sider theme="secondary" />).toJSON();
    expect(treeSecondary).toMatchSnapshot();
  });

  it('Passing props to original ant-design sider', () => {
    const treeCollapsedWidth = renderer.create(<Sider collapsedWidth={90} />).toJSON();
    expect(treeCollapsedWidth).toMatchSnapshot();

    const treeReverseArrow = renderer.create(<Sider reverseArrow />).toJSON();
    expect(treeReverseArrow).toMatchSnapshot();

    const treeCollapsible = renderer.create(<Sider collapsible />).toJSON();
    expect(treeCollapsible).toMatchSnapshot();
  });
});
