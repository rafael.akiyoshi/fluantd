import React from 'react';
import classNames from 'classnames';

const classPrefix = 'fluantd-drawer-header';
const Header = props => {
  const { className, ...restProps } = props;
  const classnames = classNames(classPrefix, className);
  return <div {...restProps} className={classnames} />;
};

export default Header;
