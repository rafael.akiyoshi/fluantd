import React from 'react';
import { shallow } from 'enzyme';
import Drawer from './drawer';

const { Header, Footer, Body } = Drawer;
const FIRST_CHILD = 0;

describe('Sample tests', () => {
  it('Should render normally', () => {
    const drawer = shallow(<Drawer />);
    expect(drawer.name()).toBe('Drawer');
    expect(drawer.getElement(FIRST_CHILD).props.className).toBe('fluantd-drawer');
  });

  it('Testing sample instance with header', () => {
    const drawer = shallow(
      <Drawer>
        <Header>
          <h2>This is a header</h2>
        </Header>
      </Drawer>
    );
    expect(drawer.children().name()).toBe('Header');
    expect(
      drawer
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with footer', () => {
    const drawer = shallow(
      <Drawer>
        <Footer>
          <h2>This is a Footer</h2>
        </Footer>
      </Drawer>
    );
    expect(drawer.children().name()).toBe('Footer');
    expect(
      drawer
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with Body', () => {
    const drawer = shallow(
      <Drawer>
        <Body>
          <h2>This is a Body</h2>
        </Body>
      </Drawer>
    );
    expect(drawer.children().name()).toBe('Body');
    expect(
      drawer
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with everything', () => {
    const drawer = shallow(
      <Drawer>
        <Header>
          <h2>This is a header</h2>
        </Header>
        <Body>
          <h2>This is a Body</h2>
        </Body>
        <Footer>
          <h2>This is a Footer</h2>
        </Footer>
      </Drawer>
    );
    const expectComponents = ['Header', 'Body', 'Footer'];
    drawer.children().forEach(node => {
      expect(expectComponents.includes(node.name())).toBe(true);
    });
  });

  it('Should receive the default props', () => {
    const drawer = shallow(<Drawer />);
    const defaultProps = {
      className: 'fluantd-drawer',
      closable: true,
      placement: 'right',
      maskClosable: true,
      width: 256,
      mask: true
    };
    Object.entries(defaultProps).forEach(entry => {
      expect(drawer.prop(entry[0])).toBe(entry[1]);
    });
  });

  it('Should receive props', () => {
    const props = {
      closable: false,
      placement: 'left',
      maskClosable: false,
      width: 512,
      mask: false
    };
    const drawer = shallow(<Drawer {...props} />);
    Object.entries(props).forEach(entry => {
      expect(drawer.prop(entry[0])).toBe(entry[1]);
    });
  });
});
