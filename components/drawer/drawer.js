import React from 'react';
import AntDrawer from 'antd/lib/drawer';
import 'antd/lib/drawer/style';
import classNames from 'classnames';
import Header from './header';
import Body from './body';
import Footer from './footer';

const classPrefix = 'fluantd-drawer';
const Drawer = props => {
  const { className, ...restProps } = props;
  const classnames = classNames(classPrefix, className);
  return <AntDrawer {...restProps} className={classnames} />;
};

Drawer.Header = Header;
Drawer.Body = Body;
Drawer.Footer = Footer;
export default Drawer;
