import React, { PureComponent } from 'react';
import AntInput from 'antd/lib/input';
import Icon from '../icon';
import 'antd/lib/input/style';

const getNextInputState = type => {
  if (type === 'password') {
    return {
      type: 'text',
      style: 'password-active'
    };
  }
  return {
    type: 'password',
    style: null
  };
};

const isPassword = type => {
  return type === 'password';
};

class Input extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      input: {
        type: props.type,
        style: null
      }
    };
  }

  toggleInput = type => {
    this.setState({
      input: {
        ...getNextInputState(type)
      }
    });
  };

  render() {
    const { input } = this.state;
    const { type, ...rest } = this.props;
    const suffix = <Icon type="eye" theme="filled" onClick={() => this.toggleInput(input.type)} />;
    return isPassword(type) ? (
      <AntInput {...rest} type={input.type} className={input.style} suffix={suffix} />
    ) : (
      <AntInput {...rest} type={input.type} className={input.style} />
    );
  }
}

const { Group, Search, TextArea } = AntInput;

Input.Group = Group;
Input.Search = Search;
Input.TextArea = TextArea;

export default Input;
