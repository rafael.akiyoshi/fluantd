import React from 'react';
import renderer from 'react-test-renderer';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Input from './input';

configure({ adapter: new Adapter() });

describe('Tests for default and password inputs', () => {
  test('Simple input with default props', () => {
    const finput = mount(<Input id="test" />);
    const input = finput.find('input');
    expect(input.hasClass('ant-input')).toBe(true);
  });
  test('password input with icon', () => {
    const props = {
      type: 'password'
    };
    const finput = mount(<Input id="test" {...props} />);
    const input = finput.find('input');
    const icon = finput.find('.ant-input-suffix i');
    expect(input).toBeTruthy();
    expect(icon.hasClass('anticon-eye')).toBe(true);
  });
});

test('Snapshot test for FInput password', () => {
  const component = renderer.create(<Input type="password" />);
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
