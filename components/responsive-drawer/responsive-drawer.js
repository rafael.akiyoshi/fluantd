import React from 'react';
import classNames from 'classnames';
import Header from './header';
import Body from './body';
import Footer from './footer';

const classPrefix = 'fluantd-responsive-drawer';
const ResponsiveDrawer = props => {
  const { className, visible, children } = props;
  const classnames = classNames(classPrefix, className, {
    collapsed: !visible
  });
  return <div className={classnames}>{children}</div>;
};
ResponsiveDrawer.defaultProps = {
  visible: false
};

ResponsiveDrawer.Header = Header;
ResponsiveDrawer.Body = Body;
ResponsiveDrawer.Footer = Footer;
export default ResponsiveDrawer;
