import React from 'react';
import { shallow } from 'enzyme';
import ResponsiveDrawer from './responsive-drawer';

const { Header, Footer, Body } = ResponsiveDrawer;
const FIRST_CHILD = 0;

describe('Sample tests', () => {
  it('Should render normally', () => {
    const drawer = shallow(<ResponsiveDrawer />);
    expect(drawer.getElement(FIRST_CHILD).props.className).toBe(
      'fluantd-responsive-drawer collapsed'
    );
  });

  it('Testing sample instance with header', () => {
    const drawer = shallow(
      <ResponsiveDrawer>
        <Header>
          <h2>This is a header</h2>
        </Header>
      </ResponsiveDrawer>
    );
    expect(drawer.children().name()).toBe('Header');
    expect(
      drawer
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with footer', () => {
    const drawer = shallow(
      <ResponsiveDrawer>
        <Footer>
          <h2>This is a Footer</h2>
        </Footer>
      </ResponsiveDrawer>
    );
    expect(drawer.children().name()).toBe('Footer');
    expect(
      drawer
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with Body', () => {
    const drawer = shallow(
      <ResponsiveDrawer>
        <Body>
          <h2>This is a Body</h2>
        </Body>
      </ResponsiveDrawer>
    );
    expect(drawer.children().name()).toBe('Body');
    expect(
      drawer
        .children()
        .children()
        .name()
    ).toBe('h2');
  });

  it('Testing sample instance with everything', () => {
    const drawer = shallow(
      <ResponsiveDrawer>
        <Header>
          <h2>This is a header</h2>
        </Header>
        <Body>
          <h2>This is a Body</h2>
        </Body>
        <Footer>
          <h2>This is a Footer</h2>
        </Footer>
      </ResponsiveDrawer>
    );
    const expectComponents = ['Header', 'Body', 'Footer'];
    drawer.children().forEach(node => {
      expect(expectComponents.includes(node.name())).toBe(true);
    });
  });

  // it('Should receive the default props', () => {
  //   const drawer = shallow(<ResponsiveDrawer />);
  //   const defaultProps = {
  //     className: 'fluantd-responsive-drawer',
  //     closable: true,
  //     placement: 'right',
  //     maskClosable: true,
  //     width: 256,
  //     mask: true
  //   };
  //   Object.entries(defaultProps).forEach(entry => {
  //     expect(drawer.prop(entry[0])).toBe(entry[1]);
  //   });
  // });

  // it('Should receive props', () => {
  //   const props = {
  //     closable: false,
  //     placement: 'left',
  //     maskClosable: false,
  //     width: 512,
  //     mask: false
  //   };
  //   const drawer = shallow(<ResponsiveDrawer {...props} />);
  //   Object.entries(props).forEach(entry => {
  //     expect(drawer.prop(entry[0])).toBe(entry[1]);
  //   });
  // });
});
