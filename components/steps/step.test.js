import React from 'react';
import renderer from 'react-test-renderer';
import Steps from './steps';

it('renders correctly veritical right steps', () => {
  const tree = renderer
    .create(
      <Steps direction="vertical" className="right" size="small" current={1}>
        <Steps.Step
          title="Calibrate Microphone "
          description="This is Calibrate Microphone  a description."
        />
        <Steps.Step
          title="In Progress"
          description="This is Calibrate Microphone  a description."
        />
        <Steps.Step title="Waiting" description="This is a description." />
      </Steps>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly veritical left steps', () => {
  const tree = renderer
    .create(
      <Steps direction="vertical" size="small" current={1}>
        <Steps.Step
          title="Calibrate Microphone "
          description="This is Calibrate Microphone  a description."
        />
        <Steps.Step
          title="In Progress"
          description="This is Calibrate Microphone  a description."
        />
        <Steps.Step title="Waiting" description="This is a description." />
      </Steps>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders correctly horizontal steps', () => {
  const tree = renderer
    .create(
      <Steps size="small" current={1}>
        <Steps.Step
          title="Calibrate Microphone "
          description="This is Calibrate Microphone  a description."
        />
        <Steps.Step
          title="In Progress"
          description="This is Calibrate Microphone  a description."
        />
        <Steps.Step title="Waiting" description="This is a description." />
      </Steps>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
