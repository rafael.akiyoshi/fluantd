/* eslint-disable prettier/prettier */
import React, { PureComponent } from 'react';
import AntSteps from 'antd/lib/steps';

class Steps extends PureComponent {
  static Step = AntSteps.Step;

  render() {
    const { ...rest } = this.props;
    return <AntSteps {...rest} />;
  }
}

export default Steps;
