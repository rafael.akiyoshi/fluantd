/* eslint-disable prettier/prettier */
// eslint-disable-line global-require
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const bowser = require('bowser');

let wavesurfer;
let microphone;

let browserName = '';
const canUseDOM = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
);
if (canUseDOM) {
  // eslint-disable-next-line global-require
  wavesurfer = require('wavesurfer.js');
  // eslint-disable-next-line global-require
  microphone = require('wavesurfer.js/dist/plugin/wavesurfer.microphone');

  const browser = bowser.getParser(window.navigator.userAgent);

  browserName = browser.getBrowserName();
}

class AudioWave extends React.Component {
  constructor(props) {
    super(props);
    const { audio, audio64 } = this.props;
    this.state = {
      waveSurfer: {},
      audio,
      audio64
    };
  }

  static getDerivedStateFromProps(props, state) {
    const base64ToArrayBuffer = base64 => {
      const binary_string = window.atob(base64);
      const len = binary_string.length;
      const bytes = new Uint8Array(len);
      for (let i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
      }
      return bytes.buffer;
    };
    const { audio64, audio, waveSurfer } = state;
    if (props.audio64 !== audio64 || props.audio !== audio) {
      if (
        Object.keys(waveSurfer).length !== 0 &&
        waveSurfer.constructor !== Object &&
        props.audio64
      ) {
        const buffer = base64ToArrayBuffer(props.audio64);
        waveSurfer.loadArrayBuffer(buffer);
      }
      return {
        audio64: props.audio64,
        audio: props.audio
      };
    }
    return null;
  }

  componentDidMount() {
    const {
      waveColor,
      progressColor,
      height,
      barWidth,
      barHeight,
      mode,
      onRef,
      waveId,
      isNormalized
    } = this.props;
    onRef(this);
    let { audio, audio64 } = this.state;
    const plugins = [];
    let interact = true;
    let audioContext = null;
    let audioScriptProcessor = null;
    let cursorWidth = 1;
    if (mode === 'record') {
      plugins.push(microphone.create());
      audio = '';
      audio64 = '';

      interact = false;
      if (browserName === 'Safari') {
        // Safari 11 or newer automatically suspends new AudioContext's that aren't
        // created in response to a user-gesture, like a click or tap, so create one
        // here (inc. the script processor)
        const AudioContext = window.AudioContext || window.webkitAudioContext;
        audioContext = new AudioContext();
        audioScriptProcessor = audioContext.createScriptProcessor(1024, 1, 1);
        cursorWidth = 0;
      }
    }

    const waveSurfer = wavesurfer.create({
      container: document.querySelector(`#${waveId}`),
      waveColor,
      progressColor,
      height,
      barWidth,
      barHeight,
      responsive: true,
      isNormalized,
      interact,
      cursorWidth,
      audioScriptProcessor,
      audioContext,
      plugins
    });

    if (audio) {
      waveSurfer.load(audio);
    } else if (audio64) {
      const bufferAudio = this.base64ToArrayBuffer(audio64);
      waveSurfer.loadArrayBuffer(bufferAudio);
    }
    this.setState({ waveSurfer });
  }

  componentWillUnmount() {
    const { onRef } = this.props;
    onRef(undefined);
  }

  base64ToArrayBuffer = base64 => {
    const binary_string = window.atob(base64);
    const len = binary_string.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  };

  getWaveSurfer = () => {
    const { waveSurfer } = this.state;
    return waveSurfer;
  };

  render() {
    const { waveId, hide, className } = this.props;
    let classes = classNames(className);
    if (hide) {
      classes = classNames('hide-audio-wave', className);
    }
    return <div id={waveId} className={classes} />;
  }
}

AudioWave.propTypes = {
  waveId: PropTypes.string,
  /**  Audio Path/link to play */
  audio: PropTypes.string,
  audio64: PropTypes.string,
  /** Audio wave visualizer color */
  waveColor: PropTypes.string,
  /** Audio wave visualizer Progressed color */
  progressColor: PropTypes.string,
  /** Audio wave div Height */
  height: PropTypes.number,
  /** Audio wave visualizer bar width */
  barWidth: PropTypes.number,
  /** Audio wave visualizer bar  hright */
  barHeight: PropTypes.number,
  /** the wave mode maybe microphone  or player */
  mode: PropTypes.string,
  /** Pass the waveSurfer API to parent */
  onRef: PropTypes.func,
  /** hide the wave */
  hide: PropTypes.bool,
  /** hide the wave */
  isNormalized: PropTypes.bool,
  /** class */
  className: PropTypes.string
};

AudioWave.defaultProps = {
  waveId: 'waveform',
  audio: '',
  audio64: '',
  waveColor: '#689bff',
  progressColor: '#364eff',
  height: 120,
  barWidth: 2,
  barHeight: 2,
  mode: 'player',
  hide: false,
  isNormalized: false
};

export default AudioWave;
