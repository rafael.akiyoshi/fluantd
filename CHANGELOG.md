# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.0"></a>
# 0.2.0 (2019-01-02)


### Bug Fixes

*  remove less import in component file ([61f8757](https://gitlab.com/wajih.tabka/fluantd/commit/61f8757))
* adding file loader to webpack ([980ea73](https://gitlab.com/wajih.tabka/fluantd/commit/980ea73))
* button bug conflict ([26c7610](https://gitlab.com/wajih.tabka/fluantd/commit/26c7610))
* conflict ([f5d6896](https://gitlab.com/wajih.tabka/fluantd/commit/f5d6896))
* dom check for server render ([10776e1](https://gitlab.com/wajih.tabka/fluantd/commit/10776e1))
* dom test server side ([1a00df9](https://gitlab.com/wajih.tabka/fluantd/commit/1a00df9))
* merging conflicts ([1f2fbdf](https://gitlab.com/wajih.tabka/fluantd/commit/1f2fbdf))
* merging conflicts ([f08e005](https://gitlab.com/wajih.tabka/fluantd/commit/f08e005))
* merging conflicts with FSideBar ([a7e06b1](https://gitlab.com/wajih.tabka/fluantd/commit/a7e06b1))
* merging with card ([5de66cd](https://gitlab.com/wajih.tabka/fluantd/commit/5de66cd))
* test config: ([0ba4b7d](https://gitlab.com/wajih.tabka/fluantd/commit/0ba4b7d))


### Features

* audio player ([46fd4b3](https://gitlab.com/wajih.tabka/fluantd/commit/46fd4b3))
* finishing message component ([3e79739](https://gitlab.com/wajih.tabka/fluantd/commit/3e79739))
* implementing steps with rigth steps ([ab005ca](https://gitlab.com/wajih.tabka/fluantd/commit/ab005ca))



<a name="0.1.0"></a>
# 0.1.0 (2018-11-20)
